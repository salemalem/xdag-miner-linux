XMRig2XDAG is a stratum proxy for Monero (XMR) miners mining XDAG coins. XMRig2XDAG is translator between XMR stratum protocol and XDAG mining protocol.

XMRig can connect to XDAG mining pool through XMRig2XDAG proxy. 

1. start up 

 xmrig2xdag -c config.json
 
 
 xmrig -c config.json  (using administrator or root)
 

2. xmrig2xdag config json file

{
  "strport": 3232,  // port for xmrig
  "url": "cn.xdagmine.com:13654",  // XDAG mining pool address and port
  "tls": false,  // using SSL
  "debug": false,  //  printing debug info
  "testnet": false,  // using test net
  "socks5": ""       // SOCKS5 proxy address:port
}

3. xmrig config json file

"pools": [
    {
        "algo": "rx/xdag", // mining XDAG
        "coin": null,
        "url": "stratum+tcp://127.0.0.1:3232", // xmrig2xdag address and port
        "user": "YOUR_WALLET_ADDRESS",  // your XDAG wallet address
        "pass": "your_miner_name",			// naming your miner
        .....
    }
]
